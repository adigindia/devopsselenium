package samplepackage;
import org.testng.annotations.Test;

import utils.BrowserCreation;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
 
import java.net.MalformedURLException;

import org.openqa.selenium.By;
import org.openqa.selenium.Rectangle;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterTest;

public class ATALogoTestNG {

	WebDriver driver; // class member - which is available to all the methods

	@Parameters({"browser","host","port"})
	@BeforeTest
	public void beforeTest(@Optional("chrome") String browser,@Optional("http://101.53.137.206") String host, @Optional("4444")String port) throws MalformedURLException {
		//driver = BrowserCreation.createRemoteDriver("chrome", "http://101.53.136.172", "4444");
		driver = BrowserCreation.createRemoteDriver(browser, host, port);
		driver.get("https://agiletestingalliance.org/");
	}


	@Test
	public void f() {
		By logo = By.xpath("/html/body/header/div/div/h1/a/img");
		WebElement weLogo = driver.findElement(logo);
		/* two ways to get the height
		 * using WebElement getRect() method
		 * or by using hidden properties or attributes
		 * height and width
		 */

		// 1st method
		Rectangle rect = weLogo.getRect();

		System.out.println("Height = " + rect.getHeight());
		System.out.println("Width = " + rect.getWidth());

		//2nd Method - by using hidden attributes width and height
		String width =  weLogo.getAttribute("width");
		String height =  weLogo.getAttribute("height");
		System.out.println("Width = " + width + " Height = " + height );

	}


	@AfterTest
	public void afterTest() {

		try {
			Thread.sleep(4000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		driver.quit();

	}

}
