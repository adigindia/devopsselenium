package samplepackage;

import org.testng.annotations.Test;

import utils.BrowserCreation;
import utils.HelperFunctions;

import org.testng.annotations.BeforeTest;

import java.net.MalformedURLException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterTest;

public class CounterWebDay3 {

	WebDriver driver;

  @Test
  public void f() {

		driver.get("http://101.53.137.206:8082/CounterWebApp/");

		By firstHeader = By.xpath("/html/body/h1[1]");

		WebElement elementHeader = driver.findElement(firstHeader);

		String strHeader = elementHeader.getText();

		System.out.println(strHeader);


		By counter = By.xpath("//*[contains(text(),\"Counter\")]");

		WebElement elementCounter = driver.findElement(counter);

		String counterText = elementCounter.getText();

		System.out.println(counterText);

		driver.get("http://101.53.137.206:8082/CounterWebApp/");

		 counter = By.xpath("//*[contains(text(),\"Counter\")]");

		 elementCounter = driver.findElement(counter);

		 counterText = elementCounter.getText();

		System.out.println(counterText);

		try {
			Thread.sleep(4000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

  }


  @BeforeTest //setup
  public void beforeTest() throws MalformedURLException {

	 // driver = HelperFunctions.createAppropriateDriver("chrome");

	  driver = BrowserCreation.createRemoteDriver("chrome","http://101.53.137.206","4444");

  }

  @AfterTest // quit - after
  public void afterTest() {

	  driver.quit();
  }

}
