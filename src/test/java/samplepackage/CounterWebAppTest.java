package samplepackage;
import org.testng.annotations.Test;

import utils.BrowserCreation;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;

import java.net.MalformedURLException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

public class CounterWebAppTest {

	WebDriver driver; // class member - which is available to all the methods

	@Parameters({"browser","host","port"})
	@BeforeTest
	public void beforeTest(@Optional("chrome") String browser,@Optional("http://101.53.137.206") String host, @Optional("4444")String port) throws MalformedURLException {
		//driver = BrowserCreation.createRemoteDriver("chrome", "http://101.53.136.172", "4444");
		driver = BrowserCreation.createRemoteDriver(browser, host, port);
	}
 

	@Test
	public void f() {

		driver.get("http://101.53.137.206:8082/CounterWebApp/");

		By byHeader1 = By.xpath("/html/body/h1[1]");
		WebElement we = driver.findElement(byHeader1);
		String strHeader = we.getText();
		System.out.println(strHeader);

		By byCounter = By.xpath("//*[contains(text(),'Counter')]");
		WebElement weCtr = driver.findElement(byCounter);
		String strCounter = weCtr.getText();
		System.out.println(strCounter);
		String subCnt = strCounter.substring(9);
		subCnt = subCnt.trim();

		System.out.println(subCnt);

		int firstCount = Integer.parseInt(subCnt);

		System.out.println(firstCount);

		//get the url once again
		driver.get("http://101.53.137.206:8082/CounterWebApp/");

		weCtr = driver.findElement(byCounter);
		strCounter = weCtr.getText();
		System.out.println(strCounter);
		subCnt = strCounter.substring(9);
		subCnt = subCnt.trim();

		int secondCount = Integer.parseInt(subCnt);

		System.out.println(secondCount);

		boolean flag = false;
		if ((firstCount+1) == secondCount)
		{
			flag = true;
			System.out.println("Counter test is pass");
		}

		//public static void assertEquals(boolean actual, boolean expected);

		Assert.assertEquals(flag,true);

	}


	@AfterTest
	public void afterTest() {

		try {
			Thread.sleep(4000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		driver.quit();

	}

}
